#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_dev_plugin_info_data_extention.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IDevPluginInfoDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IDevPluginInfoDataExtention IDataExtention)
	DATA_EXTENTION_BASE_DEFINITIONS(IDevPluginInfoDataExtention, IDevPluginInfoDataExtention, {"name", "path", "repository", "isEnabled", "updateAvailable", "hasLocalChanges"})

public:
	DataExtention(QObject* parent) :
		QObject(parent)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

public:
	QString name() override
	{
		return "No name";
	}
	QString path() override
	{
		return "";
	}
	QString repository() override
	{
		return "";
	}
	bool isEnabled() override
	{
		return true;
	}
	bool updateAvailable() override
	{
		return false;
	}
	bool hasLocalChanges() override
	{
		return false;
	}
};
